FROM python:3.8.2

MAINTAINER Luis Manuel Chavez Velasco

ENV PYTHONUNBUFFERED 1

COPY ./requirements.txt /requirements.txt
RUN pip install -r /requirements.txt

RUN mkdir /src
WORKDIR /src

ADD /app/ /src